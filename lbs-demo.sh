#!/bin/bash

# Example script usage
# ./lbs-demo.sh lbs-config.yaml

set -e

# Check if AWS CLI is installed.
function check_awscliversion {
   aws_cli_version=$(aws --version)
   echo $aws_cli_version

   #Test if aws_cli_version exists, if it does not, install it.
   if [[ -z $aws_cli_version ]]; then
      $(sudo apt install awscli)
   fi
}

# Get latest updates
function get_updates () {
   sudo apt update && sudo apt upgrade -y
}

# Parse config file and create parameters for each node
function parse_yaml () {
   local prefix=$2
   local s='[[:space:]]*' w='[a-zA-Z0-9_]*' fs=$(echo @|tr @ '\034')
   sed -ne "s|^\($s\):|\1|" \
        -e "s|^\($s\)\($w\)$s:$s[\"']\(.*\)[\"']$s\$|\1$fs\2$fs\3|p" \
        -e "s|^\($s\)\($w\)$s:$s\(.*\)$s\$|\1$fs\2$fs\3|p"  $1 |
   awk -F$fs '{
      indent = length($1)/2;
      vname[indent] = $2;
      for (i in vname) {if (i > indent) {delete vname[i]}}
      if (length($3) > 0) {
         vn=""; for (i=0; i<indent; i++) {vn=(vn)(vname[i])("_")}
         printf("%s%s%s=\"%s\"\n", "'$prefix'",vn, $2, $3);
      }
   }'
}

function verify_param () {
  [[ -z ${!1} ]] && { echo "$1 value not present in config.yaml, exiting."; exit 1; }
}

# Parse config file and create parameters
function parse_config () {
   eval $(parse_yaml $1)
}

# Get the full name based on config file parameters 
function get_full_name () {
   full_name="$tags_company-$tags_application-$tags_value"
   echo "full_name: $full_name"
}

# Wait for background process to terminate
function please_wait () {
   echo ""
   echo "Please wait..."
   process1=$1
   wait $process1
}

# Create EC2 Instance with config parameters
function create_ec2 () {
   echo ""
   echo "About to create EC2 instance."
   # Change DeleteOnTermination=false when you are done debugging.
   aws ec2 run-instances \
      --block-device-mappings "DeviceName=$ebs_name_device,Ebs={VolumeSize=$ebs_volume_size,DeleteOnTermination=true}" \
      --image-id $ami_id \
      --count $instance_count \
      --instance-type $instance_type \
      --key-name $keypair_name \
      --security-group-ids $securitygroup_id \
      --subnet-id $subnet_id \
      --tag-specifications "ResourceType=$resource_type_instance,Tags=[{Key=$tags_key,Value=$full_name}]" \
         "ResourceType=$resource_type_volume,Tags=[{Key=$tags_key,Value=$full_name$tags_suffix_data}]" \
      --user-data file://lbs-userdata.sh &

   please_wait $!
   echo "EC2 Instance has been created."
}

# Get EC2 ID
function get_ec2id_fromname () {
   ec2id=$(aws ec2 describe-instances \
   --filter Name=tag:Name,Values="$full_name" \
   --query "Reservations[].Instances[].InstanceId" --output text);
   echo "ec2_id: $ec2id"
}

# Get EC2 ID Passing in variable
function get_ec2id_fromname2 () {
   ec2id2=$(aws ec2 describe-instances \
   --filter Name=tag:Name,Values="$1" \
   --query "Reservations[].Instances[].InstanceId" --output text);
   echo "ec2_id2: $ec2id2"
}

# Create additional EBS Data (not root) volume
function create_ebs_data_volume () {
   echo ""
   echo "About to create EBS Data volume."
   aws ec2 create-volume --availability-zone $az_us_e2a \
      --size $ebs_volume_size \
      --volume-type $ebs_volume_type \
      --tag-specifications "ResourceType=$resource_type_volume,Tags=[{Key=$tags_key,Value=$full_name-$tags_datetimestamp-$tags_suffix_data}]" &
#      --tag-specifications "ResourceType=$resource_type_instance,Tags=[{Key=$tags_key,Value=$full_name}]" &
   
   please_wait $!
   echo "EBS Data volume has been created."
}

# Tag the EBS Data volume
function tag_ebs_data_volume () {
   aws ec2 create-tags \
      --resources "$volid" \
      --tags "Key=$tags_key,Value=$full_name$tags_suffix_boot"
}

# Returns all the volume Ids.
function get_volid_fromname () {
   volid=$(aws ec2 describe-instances \
      --filter Name=tag:Name,Values="$full_name" \
      --query 'Reservations[].Instances[].BlockDeviceMappings[?DeviceName==`/dev/sda1`].Ebs[].VolumeId' \
      --output text); 
   echo "volume_id: $volid"
}

# Create Launch Template
function lt_create () {
  aws ec2 create-launch-template --launch-template-name "$tags_company-$tags_application-$tags_suffix_lt-$tags_datetimestamp" \
   --version-description "$tags_version" \
   --launch-template-data "TagSpecifications=[{ResourceType=$resource_type_instance,Tags=[{Key=$tags_key,Value=$tags_company-$tags_application-$tags_suffix_lt}]}],ImageId=$ami_id,InstanceType=$instance_type,KeyName=$keypair_name,NetworkInterfaces=[{AssociatePublicIpAddress=true,DeleteOnTermination=true,DeviceIndex=0,Groups=$securitygroup_id,SubnetId=$subnet_id}],UserData=$b64" &

   please_wait $!
   echo "Launch Template has been created."
}

# Create Launch Template with EBS volume
function lt_ebs_create () {
aws ec2 create-launch-template --launch-template-name "$tags_company-$tags_application-$tags_suffix_ltebs" \
   --version-description "$tags_version" \
   --launch-template-data "TagSpecifications=[{ResourceType=$resource_type_instance,Tags=[{Key=$tags_key,Value=$tags_company-$tags_application-$tags_suffix_ltebs-1}]}],BlockDeviceMappings=[{DeviceName=$ebs_name_device,Ebs={VolumeSize=$ebs_volume_size,VolumeType=$ebs_volume_type,DeleteOnTermination=true}}],ImageId=$ami_id,InstanceType=$instance_type"
}

function convert_to_base64 () {
   b64=`base64 $1`
}

function get_ltid_fromname () {
   ltid=$(aws ec2 describe-launch-templates \
   --query 'LaunchTemplates[?DefaultVersionNumber==`1`].LaunchTemplateId' \
   --output text);
   echo "launch_template_id: $ltid"
}

# Create Launch Template with EBS volume version 2 based on version 1
function lt_ebs_v2_create () {
aws ec2 create-launch-template-version --launch-template-id "$ltid" \
   --version-description version-2 \
   --source-version 1 \
   --launch-template-data "ImageId=$ami_id"
}

# Create Autoscaling group with Launch Template
function asg_create () {
aws autoscaling create-auto-scaling-group --auto-scaling-group-name "$tags_company-$tags_application-$tags_suffix_asg" \
   --launch-template "LaunchTemplateName=$tags_company-$tags_application-$tags_suffix_lt-$tags_datetimestamp,Version=$asg_version" \
   --min-size "$asg_min" \
   --max-size "$asg_max" \
   --desired-capacity "$asg_desired" \
   --availability-zones "$az_us_e2a" 
}

function main () {
   check_awscliversion
   get_updates
   echo ""
   echo "List of config file parameters:"
   parse_yaml $1
   echo ""
   parse_config $1
   echo "Done generating config file parameters:"
   echo ""
   create_ebs_data_volume 
   volid=`aws ec2 describe-volumes --filter Name=tag:Name,Values=*data --query "Volumes[].VolumeId" --output text`
   aws ssm put-parameter --name /gitlab/volume_id --value $volid --type String --overwrite --tier Standard
   tag_ebs_data_volume
   aws ssm put-parameter --name /gitlab/role_name --value $role_name --type String --overwrite --tier Standard
   
   # Create the role and attach the trust policy that allows EC2 to assume this role.
   aws iam create-role --role-name $role_name --assume-role-policy-document file://$role_policy_document
   
   # Embed the permissions policy (in this example a managed policy) to the role 
   # to specify what it is allowed to do.
   for policy in $role_policy_list
   do
      # sed is using a regular expression to find parentheses and remove them
      policy_no_parenthesis=`echo $policy | sed 's/[()]//g'`
      aws iam attach-role-policy \
         --policy-arn $policy_no_parenthesis \
         --role-name $role_name
   done

   # Create the instance profile required by EC2 to contain the role
   aws iam create-instance-profile --instance-profile-name $instance_profile_name
   
   # Finally, add the role to the instance profile
   aws iam add-role-to-instance-profile --instance-profile-name $instance_profile_name --role-name $role_name

   convert_to_base64 lbs-userdata.sh 
   lt_create 
   get_ltid_fromname
   asg_create
   banner "The End"
}

main $1 


