#!/bin/bash

# Example script usage
# ./lbs-demo.sh lbs-config.yaml

set -e

# Check if AWS CLI is installed.
function check_awscliversion {
   aws_cli_version=$(aws --version)
   echo $aws_cli_version

   #Test if aws_cli_version exists, if it does not, install it.
   if [[ -z $aws_cli_version ]]; then
      $(sudo apt install awscli)
   fi
}

# Get latest updates
function get_updates () {
   sudo apt update && sudo apt upgrade -y
}

# Parse config file and create parameters for each node
function parse_yaml () {
   local prefix=$2
   local s='[[:space:]]*' w='[a-zA-Z0-9_]*' fs=$(echo @|tr @ '\034')
   sed -ne "s|^\($s\):|\1|" \
        -e "s|^\($s\)\($w\)$s:$s[\"']\(.*\)[\"']$s\$|\1$fs\2$fs\3|p" \
        -e "s|^\($s\)\($w\)$s:$s\(.*\)$s\$|\1$fs\2$fs\3|p"  $1 |
   awk -F$fs '{
      indent = length($1)/2;
      vname[indent] = $2;
      for (i in vname) {if (i > indent) {delete vname[i]}}
      if (length($3) > 0) {
         vn=""; for (i=0; i<indent; i++) {vn=(vn)(vname[i])("_")}
         printf("%s%s%s=\"%s\"\n", "'$prefix'",vn, $2, $3);
      }
   }'
}

function verify_param () {
  [[ -z ${!1} ]] && { echo "$1 value not present in config.yaml, exiting."; exit 1; }
}

# Parse config file and create parameters
function parse_config () {
   eval $(parse_yaml $1)
}

# Get the full name based on config file parameters 
function get_full_name () {
   full_name="$tags_company-$tags_application-$tags_value"
}

# Wait for background process to terminate
function please_wait () {
   echo ""
   echo "Please wait..."
   process1=$1
   wait $process1
}

# Create EC2 Instance with config parameters
function create_ec2 () {
   echo ""
   echo "About to create EC2 instance."
   # Change DeleteOnTermination=false when you are done debugging.
   aws ec2 run-instances \
      --block-device-mappings "DeviceName=$ebs_name_device,Ebs={VolumeSize=$ebs_volume_size_boot,DeleteOnTermination=true}" \
      --image-id $ami_id \
      --count $instance_count \
      --instance-type $instance_type \
      --key-name $keypair_name \
      --security-group-ids $secgrpid\
      --subnet-id $subnid1\
      --tag-specifications "ResourceType=$resource_type_instance,Tags=[{Key=$tags_key,Value=$full_name}]" \
         "ResourceType=$resource_type_volume,Tags=[{Key=$tags_key,Value=$full_name$tags_suffix_data}]" \
      --user-data file://lbs-userdata.sh &

   please_wait $!
   echo "EC2 Instance has been created."
}

# Get EC2 ID
function get_ec2id_fromname () {
   ec2id=$(aws ec2 describe-instances \
   --filter Name=tag:Name,Values="$full_name" \
   --query "Reservations[].Instances[].InstanceId" --output text);
   echo "ec2_id: $ec2id"
}

# Get EC2 ID Passing in variable
function get_ec2id_fromname2 () {
   ec2id2=$(aws ec2 describe-instances \
   --filter Name=tag:Name,Values="$1" \
   --query "Reservations[].Instances[].InstanceId" --output text);
   echo "ec2_id2: $ec2id2"
}

# Create additional EBS Data (not root) volume
function create_ebs_data_volume () {
   echo ""
   echo "About to create EBS Data volume."
   aws ec2 create-volume --availability-zone $az_us_e2a \
      --size $ebs_volume_size_data \
      --volume-type $ebs_volume_type \
      --tag-specifications "ResourceType=$resource_type_volume,Tags=[{Key=$tags_key,Value=$full_name-$tags_suffix_data$test}]" \
      --region $region_useast2 &
   
   please_wait $!
   echo "EBS Data volume has been created."
}

# Tag the EBS Boot volume
function tag_ebs_boot_volume () {
   aws ec2 create-tags \
      --resources "$volid" \
      --tags "Key=$tags_key,Value=$full_name-$tags_suffix_boot$test"
}

# Returns all the volume Ids.
function get_volid_fromname () {
   volid=$(aws ec2 describe-instances \
      --filter Name=tag:Name,Values="$full_name" \
      --query 'Reservations[].Instances[].BlockDeviceMappings[?DeviceName==`/dev/sda1`].Ebs[].VolumeId' \
      --output text); 
   echo "volume_id: $volid"
}

# Create Launch Template
      # TODO apply 'boot' tag
      # TODO correctly change boot volume to 30 GB instead of 8 GB - DONE
function lt_create () {
  aws ec2 create-launch-template --launch-template-name "$full_name-$tags_suffix_lt$test" \
   --version-description "$tags_version" \
   --region "$region_useast2" \
   --launch-template-data "TagSpecifications=[{ResourceType=$resource_type_instance,Tags=[{Key=$tags_key,Value=$full_name-$tags_suffix_lt$test}]}],ImageId=$ami_id_uswest1,InstanceType=$instance_type,KeyName=$keypair_name,NetworkInterfaces=[{AssociatePublicIpAddress=true,DeleteOnTermination=true,DeviceIndex=0,Groups=$secgrpid,SubnetId=$subnid1}],BlockDeviceMappings=[{DeviceName=$ebs_name_root,Ebs={VolumeSize=$ebs_volume_size_boot,VolumeType=$ebs_volume_type,DeleteOnTermination=true}}],IamInstanceProfile={Arn=arn:aws:iam::128611931852:instance-profile/LBS-Platform-RBaccess},UserData=$b64" \
   --tag-specifications "ResourceType=$resource_type_lt,Tags=[{Key=$tags_key,Value=$full_name-$tags_suffix_lt$test}]" & 

   please_wait $!
   echo "Launch Template has been created." >> sanitycheck.txt
}

# Create Launch Template with EBS volume
function lt_ebs_create () {
aws ec2 create-launch-template --launch-template-name "$tags_company-$tags_application-$tags_suffix_ltebs" \
   --version-description "$tags_version" \
   --launch-template-data "TagSpecifications=[{ResourceType=$resource_type_instance,Tags=[{Key=$tags_key,Value=$tags_company-$tags_application-$tags_suffix_ltebs-1}]}],BlockDeviceMappings=[{DeviceName=$ebs_name_device,Ebs={VolumeSize=$ebs_volume_size_boot,VolumeType=$ebs_volume_type,DeleteOnTermination=true}}],ImageId=$ami_id,InstanceType=$instance_type"
}

function convert_to_base64 () {
   b64=`base64 $1`
}

function get_ltid () {
   ltid=`aws ec2 describe-launch-templates --filters "Name=launch-template-name,Values=$full_name-$tags_suffix_lt$test" --query "LaunchTemplates[].LaunchTemplateId" --output text`
   echo "LaunchTemplateId: $ltid"
}

function get_lt_default_version () {
   ltdefver=`aws ec2 describe-launch-templates --filters "Name=launch-template-name,Values=$full_name-$tags_suffix_lt$test" --query "LaunchTemplates[].DefaultVersionNumber" --output text`
   echo "LaunchTemplate DefaultVersionNumber: $ltdefver"
}

# Create Launch Template with EBS volume version 2 based on version 1
function lt_ebs_v2_create () {
aws ec2 create-launch-template-version --launch-template-id "$ltid" \
   --version-description version-2 \
   --source-version 1 \
   --launch-template-data "ImageId=$ami_id"
}

# Create Autoscaling group with Launch Template
function asg_create () {
aws autoscaling create-auto-scaling-group --auto-scaling-group-name "$full_name-$tags_suffix_asg$test" \
   --launch-template "LaunchTemplateName=$full_name-$tags_suffix_lt$test,Version=$lt_version_default" \
   --min-size "$asg_min" \
   --max-size "$asg_max" \
   --desired-capacity "$asg_desired" \
   --target-group-arns "$tgarn" \
   --availability-zones "$az_us_e2a" 
   echo "Created Autoscaling group with Launch Template"
}

function main () {
   check_awscliversion
   get_updates
   echo ""

   echo "List of config file parameters:"   
   parse_yaml $1
   echo ""

   parse_config $1
   echo "Done generating config file parametersr."
   echo ""

   get_full_name
   echo "full_name=tags_company-tags_application-tags_value: $full_name"

   # Check if the VPC exists
   vpcid=`aws ec2 describe-vpcs --filters "Name=tag:Name,Values=$tags_company-$tags_suffix_vpc$test" --output text`
    echo $vpcid
    countvpc=$(echo "$vpcid" | grep -o 'vpc*' | wc -l)
    echo "Number of VPCs found: $countvpc"

    # If the countvpc is greater than zero then there are multiple VPCs
    if [[ $countvpc > 0 ]]; then
       echo "Warning: There are multiple VPCs."
    fi

    # Test if the variable is 0
    if [[ $countvpc -eq 0 ]]; then 
   
      # VPC
      aws ec2 create-vpc --cidr-block $vpc_cidr --tag-specifications "ResourceType=$resource_type_vpc,Tags=[{Key=$tags_key,Value=$tags_company-$tags_suffix_vpc$test}]" --region $region_useast2
      echo "vpc name: $tags_company-$tags_suffix_vpc$test"
      vpcid=`aws ec2 describe-vpcs --filters "Name=tag:Name,Values=$tags_company-$tags_suffix_vpc$test" --query "Vpcs[].VpcId" --output text`
      echo "vpcid: $vpcid"
      aws ec2 modify-vpc-attribute --vpc-id $vpcid --enable-dns-hostnames

      # Internet Gateway
      aws ec2 create-internet-gateway --tag-specifications "ResourceType=$resource_type_ig,Tags=[{Key=$tags_key,Value=$full_name-$tags_suffix_igw$test}]" --region $region_useast2
      igwid=`aws ec2 describe-internet-gateways --filters "Name=tag:Name,Values=$full_name-$tags_suffix_igw$test" --query "InternetGateways[].InternetGatewayId" --output text`
      echo "igwid: $igwid"
      aws ec2 attach-internet-gateway --vpc-id $vpcid --internet-gateway-id $igwid

      # Subnet
      aws ec2 create-subnet --vpc-id $vpcid --cidr-block $cidr_public --availability-zone $az_us_e2a --tag-specifications "ResourceType=$resource_type_subn,Tags=[{Key=$tags_key,Value='$az_us_e2a - $cidr_public$test'}]" --region $region_useast2
      aws ec2 create-subnet --vpc-id $vpcid --cidr-block $cidr_private --availability-zone $az_us_e2b --tag-specifications "ResourceType=$resource_type_subn,Tags=[{Key=$tags_key,Value='$az_us_e2b - $cidr_private$test'}]" --region $region_useast2
      subnid1=`aws ec2 describe-subnets --filters "Name=tag:Name,Values='$az_us_e2a - $cidr_public$test'" --query "Subnets[].SubnetId" --output text` 
      echo "subnid1: $subnid1"
      subnid2=`aws ec2 describe-subnets --filters "Name=tag:Name,Values='$az_us_e2b - $cidr_private$test'" --query "Subnets[].SubnetId" --output text` 
      echo "subnid2: $subnid2"

      # Security Group
      aws ec2 create-security-group --group-name "$full_name-$tags_suffix_sg$test" --description "$securitygroup_description" --vpc-id "$vpcid" --tag-specifications "ResourceType=$resource_type_sg,Tags=[{Key=$tags_key,Value=$full_name-$tags_suffix_sg$test}]" --region $region_useast2
      secgrpid=`aws ec2 describe-security-groups --filters "Name=tag:Name,Values=$full_name-$tags_suffix_sg$test" --query "SecurityGroups[].GroupId" --output text`
      aws ec2 authorize-security-group-ingress --group-id $secgrpid --protocol $protocol_tcp --port $port_http --cidr $cidr_all
      aws ec2 authorize-security-group-ingress --group-id $secgrpid --protocol $protocol_tcp --port $port_https --cidr $cidr_all
      aws ec2 authorize-security-group-ingress --group-id $secgrpid --protocol $protocol_tcp --port $port_tcp --cidr $cidr_all
      echo "secgrpid: $secgrpid"

      # Route Tables
      rtid=`aws ec2 describe-route-tables --filters "Name=$literal_string_vpcid,Values=$vpcid" --query 'RouteTables[].RouteTableId' --output text`
      echo "rtid: $rtid"
      aws ec2 create-tags --resources "$rtid" --tags "Key=$tags_key,Value=$full_name-$tags_suffix_rt$test"
      aws ec2 create-route --route-table-id "$rtid" --destination-cidr-block "$cidr_all" --gateway-id "$igwid"
      aws ec2 associate-route-table --route-table-id "$rtid" --subnet-id "$subnid1"
   fi

   # Volumes
   # Checking if "data" Volume exists
   #volid=`aws ec2 describe-volumes --filter Name=tag:Name,Values=*data* --query "Volumes[].VolumeId" --output text`
   volid=`aws ec2 describe-volumes --filter Name=tag:Name,Values=$full_name-$tags_suffix_data$test --query "Volumes[].VolumeId" --output text`
   count=$(echo "$volid" | grep -o 'vol*' | wc -l) 
   echo "Number of 'data' Volumes found: $count"

   if [[ $count -eq 1 ]]; then
      echo "Volume ID: $volid"
   fi

   # If the count is greater than zero then there are multiple "data" volumes
   if [[ $count > 1 ]]; then
      echo "Warning: There are multiple 'data' volumes."
   fi

   # Volume Test. If the variable is 0 the 'data' volume does not exist and it needs to be created.
   if [[ $count -eq 0 ]]; then
      create_ebs_data_volume 
      #volid=`aws ec2 describe-volumes --filter Name=tag:Name,Values=*data* --query "Volumes[].VolumeId" --output text`
      volid=`aws ec2 describe-volumes --filter Name=tag:Name,Values=$full_name-$tags_suffix_data$test --query "Volumes[].VolumeId" --output text`
      
      # Parameter Store
      aws ssm put-parameter --name /gitlab/volume_id --value $volid --type String --overwrite --tier Standard
      echo "Parameter Store updated for volume_id."
      aws ssm put-parameter --name /gitlab/role_name --value $role_name --type String --overwrite --tier Standard
      echo "Parameter Store updated for role_name."
      aws ssm put-parameter --name /gitlab/region --value $region_useast2 --type String --overwrite --tier Standard
      echo "Parameter Store updated for region_useast2."
      aws ssm put-parameter --name /gitlab/tags_application --value $tags_application --type String --overwrite --tier Standard
      echo "Parameter Store updated for tags_application."
      aws ssm put-parameter --name /gitlab/domain_host --value $domain_host --type String --overwrite --tier Standard
      echo "Parameter Store updated for domain_host."

      # IAM Roles and Policy
      # Check if the role exists
      aws iam get-role --role-name "$role_name" >/dev/null 2>&1
      if [[ $? -ne 0 ]]; then
         # Create the role and attach the trust policy that allows EC2 to assume this role.
         aws iam create-role --role-name $role_name --assume-role-policy-document file://$role_policy_document

         # Embed the permissions policy (in this example a managed policy) to the role 
         # to specify what it is allowed to do.
         for policy in $role_policy_list
         do
            # sed is using a regular expression to find parentheses and remove them
            policy_no_parenthesis=`echo $policy | sed 's/[()]//g'`
            aws iam attach-role-policy \
               --policy-arn $policy_no_parenthesis \
               --role-name $role_name
         done

         # Create the instance profile required by EC2 to contain the role
         aws iam create-instance-profile --instance-profile-name $instance_profile_name
   
         # Finally, add the role to the instance profile
         aws iam add-role-to-instance-profile --instance-profile-name $instance_profile_name --role-name $role_name
      # End of IAM Roles and Policy
      fi
   #End of Volume Test
   fi   

   # Application Load Balancer (ALB)
   aws elbv2 create-load-balancer --name "$full_name-$tags_suffix_alb$test" --subnets $subnid1 $subnid2 --security-groups $secgrpid 
   albarn=`aws elbv2 describe-load-balancers --names "$full_name-$tags_suffix_alb$test" --query "LoadBalancers[].LoadBalancerArn" --output text`
   echo "albarn: $albarn"
   albname=`aws elbv2 describe-load-balancers --load-balancer-arns $albarn --query "LoadBalancers[0].DNSName" --output text`
   echo "albname: $albname"

   # Target Group
   aws elbv2 create-target-group --name "$full_name-$tags_suffix_tg$test" --protocol $protocol_HTTP --port $port_http --vpc-id $vpcid --matcher "HttpCode=$tg_healthchecks_successcodes"
   tgarn=`aws elbv2 describe-target-groups --names "$full_name-$tags_suffix_tg$test" --query "TargetGroups[].TargetGroupArn" --output text`
   echo "tgarn: $tgarn"

   # TODO update certifiate to create both *.pages.leadingbit.net AND *.leadingbit.net
   # Check if certificate exists
   certarn=`aws acm list-certificates --region $region_useast2 --query "CertificateSummaryList[?DomainName=='$domain_name'].CertificateArn" --output text`
   echo "certarn: $certarn"

   # -n tests if the length of the string in $certarn is non-zero.
   #if [[ -n "$certarn" ]]; then
   #   echo "Certificate already exists for $domain_name. No action needed."
   #else
      # Request new certificate
   #   echo "Requesting certificate for $domain_name..."
   #   certarn=`aws acm request-certificate --domain-name $domain_name --validation-method $cert_valmeth_dns --region $region_useast2 --query CertificateArn --output text`
   #   echo "certarn: $certarn"
      # Add tags to certificate
   #   aws acm add-tags-to-certificate --certificate-arn $certarn --tags "Key=$tags_key,Value=$full_name-$cert_name$test" --region $region_useast2   
   #fi

   # Check if Gitlab pages certificate exists
   pagescertarn=`aws acm list-certificates --region $region_useast2 --query "CertificateSummaryList[?DomainName=='$domain_pagesname'].CertificateArn" --output text`
   echo "pagescertarn: $pagescertarn"

   # -n tests if the length of the string in $pagescertarn is non-zero.
   #if [[ -n "$pagescertarn" ]]; then
   #then
   #   echo "Gitlab Pages Certificate already exists for $domain_pagesname. No action needed."
   #else
      # Request new Gitlab pages certificate
   #   echo "Requesting certificate for $domain_pagesname..."
   #   pagescertarn=`aws acm request-certificate --domain-name $domain_pagesname --validation-method $cert_valmeth_dns --region $region_useast2 --query CertificateArn --output text`
   #   echo "pagescertarn: $pagescertarn"
      # Add tags to Gitlab Pages Certificate
   #   aws acm add-tags-to-certificate --certificate-arn $pagescertarn --tags "Key=$tags_key,Value=$full_name-$cert_pagesname$test" --region $region_useast2   
   #fi

   if [[ $certarn -eq 0 ]]; then
      certarn=$pagescertarn
   fi

   # Create listener to ALB which also adds the certificate to ALB
   aws elbv2 create-listener --load-balancer-arn $albarn --protocol $protocol_HTTPS --port $port_https --ssl-policy "$cert_sslpolicy" --default-actions "Type=$alb_listeners_type,TargetGroupArn=$tgarn" --certificates "CertificateArn=$certarn"

   # Route 53
   # Example, $domain_host_zone = leadingbit.net
   # Example, $domain_host = code-rb.leadingbit.net
   hostedzoneid=`aws route53 list-hosted-zones --query "HostedZones[?Name=='$domain_host_zone.'].Id" --output text`
   echo "hostedzoneid: $hostedzoneid"

   if aws route53 list-resource-record-sets --hosted-zone-id $hostedzoneid --query "ResourceRecordSets[?Name == '$domain_host.'][0]" >/dev/null 2>&1; then
      echo "Resource record set [name='$domain_host.', type='$route53_resourcerecordset_type_cname'] already exists. Updating the Value/Route trafficto parameter with the Load Balancer DNS Name."
      aws route53 change-resource-record-sets --hosted-zone-id $hostedzoneid --change-batch '{
         "Changes": [
            {
               "Action": "'"$route53_action_upsert"'",
               "ResourceRecordSet": {
                  "Name": "'"$domain_host"'",
                  "Type": "'"$route53_resourcerecordset_type_cname"'",
                  "TTL": '$domain_ttl',
                  "ResourceRecords": [
                     {
                        "Value": "'"$albname"'"
                     }
                  ]
               }
            }
         ]
}'   
   else
      aws route53 change-resource-record-sets --hosted-zone-id $hostedzoneid --change-batch '{
         "Changes": [
            {
               "Action": "'"$route53_action_create"'",
               "ResourceRecordSet": {
                  "Name": "'"$domain_host"'",
                  "Type": "'"$route53_resourcerecordset_type_cname"'",
                  "TTL": '$domain_ttl',
                  "ResourceRecords": [
                     {
                        "Value": "'"$albname"'"
                     }
                  ]
               }
            }
         ]
}'   
   fi

   # Launch Template
   convert_to_base64 lbs-userdata.sh 
   lt_create 
   get_ltid

   # Autoscaling Group
   asg_create

   banner "The End"
}

function config_vars () {
   echo "List of config file parameters:"   
   parse_yaml $1
   echo ""

   parse_config $1
   echo "Done generating config file parametersr."
   echo ""

   get_full_name
   echo "full_name=tags_company-tags_application-tags_value: $full_name"
}

main $1 
#config_vars $1
