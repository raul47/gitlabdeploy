#!/bin/bash

apt update && apt upgrade -y
apt install awscli -y

echo "Inside the Userdata script." >> sanitycheck.txt
date >> sanitycheck.txt
echo "Pause 60 seconds to make sure permissions propagate." >> sanitycheck.txt
sleep 60
date >> sanitycheck.txt

# Find current EC2 Region
region_useast2=`curl -s http://169.254.169.254/latest/meta-data/placement/availability-zone | sed 's/.$//'`
echo "region_useast2 value is: " $region_useast2 >> sanitycheck.txt

# Find current EC2 ID
ec2id=`curl -s http://169.254.169.254/latest/meta-data/instance-id`
#ec2id=`aws ec2 describe-instances --filters "Name=tag:Name,Values=$full_name-$tags_suffix_lt$test" "Name=instance-state-name,Values=running" --query 'Reservations[].Instances[].InstanceId' --output text`
echo "ec2id " $ec2id >> sanitycheck.txt

# Get Role name from Parameter Store 
instanceprofilename=`aws ssm get-parameter --name /gitlab/role_name --query 'Parameter.Value' --region $region_useast2 --output text`
echo "instanceprofilename: " $instanceprofilename >> sanitycheck.txt

# Get Application name from Parameter Store 
tags_application=`aws ssm get-parameter --name /gitlab/tags_application --query 'Parameter.Value' --region $region_useast2 --output text`
echo "tags_application: " $tags_application >> sanitycheck.txt

# Get Domain Host name from Parameter Store 
domain_host=`aws ssm get-parameter --name /gitlab/domain_host --query 'Parameter.Value' --region $region_useast2 --output text`
echo "domain_host: " $domain_host >> sanitycheck.txt

# 05FEB23 - Instance Profile added in Launch Template
# Add Instance Profile to EC2 instance 
aws ec2 associate-iam-instance-profile \
   --instance-id "$ec2id" \
   --iam-instance-profile "Name=$instanceprofilename"
echo "aws ec2 associate-iam-instance-profile, exit status: $?" >> sanitycheck.txt

# Create temporary set of credentials for instalation
aws sts assume-role --role-arn arn:aws:iam::128611931852:role/LBS-Platform-RBaccess --role-session-name "RoleSession1" > assume-role-output.txt
echo "aws sts assume-role, exit status: $?" >> sanitycheck.txt

credfile=assume-role-output.txt

AccessKeyId=`grep AccessKeyId $credfile | cut -d ":" -f 2 | sed 's/[", ]//g'`
SecretAccessKey=`grep SecretAccessKey $credfile | cut -d ":" -f 2 | sed 's/[", ]//g'`
SessionToken=`grep SessionToken $credfile | cut -d ":" -f 2 | sed 's/[", ]//g'`

export AWS_ACCESS_KEY_ID=$AccessKeyId
export AWS_SECRET_ACCESS_KEY=$SecretAccessKey
export AWS_SESSION_TOKEN=$SessionToken
export AWS_DEFAULT_REGION=$region_useast2

# Pause 60 seconds to make sure permissions propagate. 
echo "Pause 60 seconds to make sure permissions propagate." >> sanitycheck.txt
date >> sanitycheck.txt
sleep 60
date >> sanitycheck.txt; echo ""

# Find Launch Template (LT) Name
ltname=`aws ec2 describe-launch-templates --filters Name=launch-template-name,Values=*gitlab* --query LaunchTemplates[].LaunchTemplateName --region $region_useast2 --output text`

# Find Launch Template (LT) Version
ltversion=`aws ec2 describe-launch-templates --filters Name=launch-template-name,Values=*gitlab* --query LaunchTemplates[].DefaultVersionNumber --region $region_useast2 --output text`

# Write EC2 Instance name
#aws ec2 create-tags \
#   --resources $ec2id \
#   --tag Key=Name,Value="$ltname-$ltversion"

# Check if EBS "data" volume exists
aws ssm get-parameter --name /gitlab/volume_id --query 'Parameter.Value' --region $region_useast2 --output text
if [ "$?" -eq 0 ]; then 
   # Install Docker Engine on EBS "boot" volume
   #curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /usr/share/keyrings/docker-archive-keyring.gpg
   #echo "deb [arch=amd64 signed-by=/usr/share/keyrings/docker-archive-keyring.gpg] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null
   #apt-get install apt-transport-https ca-certificates curl gnupg lsb-release -y
   apt-get update -y
   #apt-get install docker-ce docker-ce-cli docker-compose containerd.io -y
   apt-get install docker.io docker-compose -y

   # Find Volume ID
   volid=`aws ec2 describe-volumes --filter Name=tag:Name,Values=*data* --query "Volumes[].VolumeId" --region $region_useast2 --output text`
   echo "Before the aws ec2 wait volume-available" >> sanitycheck.txt
   date >> sanitycheck.txt
   aws ec2 wait volume-available --volume-ids $volid --region $region_useast2
   echo "Volume is now available" >> sanitycheck.txt
   date >> sanitycheck.txt

   # Now that the volume is available, you can attach the existing "data" volume.
   aws ec2 attach-volume --device /dev/sdf --instance-id $ec2id --volume-id $volid --region $region_useast2
   # Check if the EBS volume needs a file system and create one if needed
   while [ ! -e /dev/nvme1n1 ]; do sleep 1; done
   if ! file -s /dev/nvme1n1 | grep -q 'filesystem'; then
      mkfs -t xfs /dev/nvme1n1
   fi
   
   # Pause 120 seconds to make sure attached volume is ready. 
   # Note: If you do this too early you will not be able to mount correctly in the next steps.
   echo "Pause 120 seconds to make sure attached volume is ready." >> sanitycheck.txt
   date >> sanitycheck.txt
   sleep 120
   date >> sanitycheck.txt; echo ""

   # Create Application directory
   #mkdir /gitlab
   mkdir /$tags_application

   # Mount the "data" volume
   #mount /dev/nvme1n1 /gitlab/
   mount /dev/nvme1n1 /$tags_application

   # Create and start docker container
   #docker run --detach \
   #   --hostname dev-rb.leadingbit.net \
   #   --publish 443:443 --publish 80:80 \
   #   --name devrb \
   #   --restart always \
   #   --volume /gitlab/config:/etc/gitlab \
   #   --volume /gitlab/logs:/var/log/gitlab \
   #   --volume /gitlab/data:/var/opt/gitlab \
   #   --shm-size 256m \
   #   gitlab/gitlab-ce:latest
   
   # Create docker-compose.yml for GitLab

cat > /$tags_application/docker-compose.yml <<'EOL'
version: '3.6'
services:
  web:
    image: 'gitlab/gitlab-ee:latest'
    restart: always
    hostname: 'code-rb.leadingbit.net'
    container_name: gitlab
    environment:
      GITLAB_OMNIBUS_CONFIG: |
        external_url 'http://code-rb.leadingbit.net'
        # Add any other gitlab.rb configuration here, each on its own line
        gitlab_rails['smtp_enable'] = true
        gitlab_rails['smtp_address'] = 'email-smtp.us-west-1.amazonaws.com'
        gitlab_rails['smtp_port'] = 465
        gitlab_rails['smtp_user_name'] = 'AKIAR34O6K3GM35XAOOZ'
        gitlab_rails['smtp_password'] = 'BNF9m0oykQPNVVGnoaifF8nS9gA8a14VYiLa0WRS/0dn'
        gitlab_rails['smtp_authentication'] = 'login'
        gitlab_rails['smtp_ssl'] = true
        gitlab_rails['smtp_force_ssl'] = true
        gitlab_rails['smtp_enable_starttls_auto'] = true
        gitlab_rails['gitlab_email_from'] = 'no_reply@leadingbit.com'
        gitlab_pages['enable'] = true
        #nginx['listen_addresses'] = ['10.0.1.193']
        #gitlab_pages['external_http'] = ['10.0.1.194:80']
        #gitlab_pages['external_https'] = [10.0.1.194:443]
        pages_external_url "https://pages.leadingbit.net"
        #pages_external_url "http://leadingbit.net"
        #pages_nginx['enable'] = false
        pages_nginx['enable'] = true
        pages_nginx['listen_port'] = 80
        pages_nginx['listen_https'] = false
        pages_nginx['redirect_http_to_https'] = true
        grafana['enable'] = true
    ports:
      - '80:80'
      - '443:443'
      - '2222:22'
    volumes:
      - '/gitlab/config:/etc/gitlab'
      - '/gitlab/logs:/var/log/gitlab'
      - '/gitlab/data:/var/opt/gitlab'
    shm_size: '256m'
    networks:
      - gitlab
  gitlab-runner:
    image: gitlab/gitlab-runner:alpine
    container_name: gitlab-runner
    restart: always
    depends_on:
      - web
    volumes:
      - /var/run/docker.sock:/var/run/docker.sock
      - '/gitlab/gitlab-runner:/etc/gitlab-runner'
    networks:
      - gitlab

networks:
  gitlab:
    name: gitlab-network
EOL

   # Start GitLab container
   #cd /gitlab
   cd /$tags_application
   sudo docker-compose up -d

   # Pause 180 seconds to make sure container can be used. 
   echo "Pause 180 seconds to make sure container can be used." >> ../sanitycheck.txt
   date >> ../sanitycheck.txt
   sleep 180
   date >> ../sanitycheck.txt; echo ""
   echo "Gitlab is ready." >> ../sanitycheck.txt;
fi
#else
   # Put an xfs file system on attached EBS data volume the first time only
   # mkfs -t xfs /dev/nvme1n1
#fi
