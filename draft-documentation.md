GitLab Deployment Script Documentation
--------------------------------------

This Bash script deploys a GitLab instance on an AWS EC2 instance using Docker Compose. It creates a VPC with internet access, an Auto Scaling Group (ASG) with a launch template, and an Application Load Balancer (ALB). It also sets up the required security group, IAM policy, and a Route53 DNS record.

### Prerequisites

1.  AWS CLI installed and configured with appropriate access keys and secret keys.
2.  An existing key pair for your EC2 instances.
3.  An existing Route53 hosted zone.

### Script Variables

Before running the script, replace the following variables with your appropriate values:

1.  `<your_key_pair>`: The name of your existing AWS key pair for your EC2 instances. Example: `my-key-pair`.
2.  `$VPC_CIDR`: The CIDR block for your VPC. Example: `"10.0.0.0/16"`.
3.  `$SUBNET_CIDR`: The CIDR block for your subnet. Example: `"10.0.1.0/24"`.
4.  `$EBS_SIZE`: The size (in GiB) of the EBS volume for your GitLab data. Example: `20`.
5.  `$EBS_IOPS`: The IOPS (input/output operations per second) for your EBS volume. Example: `100`.
1.  `$CERT_DOMAIN`: The domain name for your SSL certificate. Example: `gitlab.example.com`.
2.  `$ROUTE53_ZONE_ID`: The ID of the hosted zone in Route53 where you want to create the DNS record. Example: `ZABCDEFGHIJKLMN`.
3.  `$DNS_NAME`: The DNS name to be used for the GitLab URL and Route53 record. Example: `gitlab.example.com`.

### Running the Script

1.  Save the script to a file, e.g., `deploy_gitlab.sh`.
2.  Update the script with your specific variable values mentioned above.
3.  Make the script executable: `chmod +x deploy_gitlab.sh`.
4.  Run the script: `./deploy_gitlab.sh`.

### What the Script Does

1.  Creates a VPC with an Internet Gateway.
2.  Creates a subnet within the VPC.
3.  Sets up a security group with required ingress rules for web access and Secure Shell (SSH) access.
4.  Creates an IAM role and policy with permissions for Amazon SSM and EBS volume management.
5.  Creates a launch template that deploys an Ubuntu-based EC2 instance with Docker and Docker Compose installed.
6.  Attaches the EBS volume to the instance and creates a file system if needed.
7.  Sets up GitLab using Docker Compose and stores data on the attached EBS volume.
8.  Creates an Auto Scaling Group with the launch template.
9.  Creates an Application Load Balancer and a target group.
10. Requests an SSL certificate using AWS Certificate Manager and attaches it to the ALB listener.
11. Creates a DNS entry (CNAME record) in Route53 pointing to the ALB.

After running the script, you should have a fully functioning GitLab instance deployed on AWS with an SSLcertificate and a Route53 DNS record. You can access your GitLab instance using the DNS name you provided (e.g., `https://gitlab.example.com`). The instance will be running within an Auto Scaling Group and will be accessible through the Application Load Balancer, providing a scalable and resilient setup.

Please note that you may need to wait a few minutes for the DNS record to propagate and the SSL certificate to be issued and validated before you can access the GitLab instance using the provided DNS name.

### Maintenance and Data Persistence

The GitLab data is stored on an EBS volume, ensuring data persistence even if the instance is terminated or replaced. When a new instance is launched by the Auto Scaling Group, it will automatically mount the EBS volume, allowing GitLab to resume its operation with the existing data.

### Important Notes

-   The script provisions resources that may incur costs in your AWS account. Make sure to review the AWS pricing for EC2, EBS, ALB, and Route53 before running the script.
-   This script provides a starting point for deploying a GitLab instance on AWS. Depending on your specific use case, you may need to further customize the script or configure additional AWS resources (e.g., Auto Scaling policies, health checks, monitoring, etc.).
-   The security group created by this script allows web access (ports 80 and 443) from any IP address (0.0.0.0/0). You may want to restrict access further based on your security requirements.
