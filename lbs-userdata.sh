#!/bin/bash

apt update && apt upgrade -y
apt install awscli -y

# Find current EC2 ID
ec2id=`curl -s http://169.254.169.254/latest/meta-data/instance-id`
echo "ec2id " $ec2id >> sanitycheck.txt

# Get Role name from Parameter Store 
#instanceprofilename=`aws ssm get-parameter --name /gitlab/role_name --query 'Parameter.Value' --region us-east-2 --output text`

# 05FEB23 - Instance Profile added in Launch Template
# Add Instance Profile to EC2 instance 
#aws ec2 associate-iam-instance-profile \
#   --instance-id $ec2id \
#   --iam-instance-profile Name=$instanceprofilename

# Pause 60 seconds to make sure permissions propagate. 
echo "Pause 60 seconds to make sure permissions propagate." >> sanitycheck.txt
date >> sanitycheck.txt
sleep 60
date >> sanitycheck.txt; echo ""

# Create temporary set of credentials for instalation
aws sts assume-role --role-arn arn:aws:iam::128611931852:role/LBS-Platform-RBaccess --role-session-name "RoleSession1" > assume-role-output.txt

credfile=assume-role-output.txt

AccessKeyId=`grep AccessKeyId $credfile | cut -d ":" -f 2 | sed 's/[", ]//g'`
SecretAccessKey=`grep SecretAccessKey $credfile | cut -d ":" -f 2 | sed 's/[", ]//g'`
SessionToken=`grep SessionToken $credfile | cut -d ":" -f 2 | sed 's/[", ]//g'`
MyRegion=us-east-2

export AWS_ACCESS_KEY_ID=$AccessKeyId
export AWS_SECRET_ACCESS_KEY=$SecretAccessKey
export AWS_SESSION_TOKEN=$SessionToken
export AWS_DEFAULT_REGION=$MyRegion

# Find Launch Template (LT) Name
ltname=`aws ec2 describe-launch-templates --filters Name=launch-template-name,Values=*gitlab* --query LaunchTemplates[].LaunchTemplateName --region us-east-2 --output text`

# Find Launch Template (LT) Version
ltversion=`aws ec2 describe-launch-templates --filters Name=launch-template-name,Values=*gitlab* --query LaunchTemplates[].DefaultVersionNumber --region us-east-2 --output text`

# Write EC2 Instance name
aws ec2 create-tags \
   --resources $ec2id \
   --tag Key=Name,Value=$ltname-v$ltversion

# Check if EBS "data" volume exists
aws ssm get-parameter --name /gitlab/volume_id --query 'Parameter.Value' --region us-east-2 --output text
if [ "$?" -eq 0 ]; then 
   # Install Docker Engine on EBS "boot" volume
   curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /usr/share/keyrings/docker-archive-keyring.gpg
   echo "deb [arch=amd64 signed-by=/usr/share/keyrings/docker-archive-keyring.gpg] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null
   apt-get install apt-transport-https ca-certificates curl gnupg lsb-release -y
   apt-get update
   apt-get install docker-ce docker-ce-cli containerd.io -y
   docker pull gitlab/gitlab-ce:latest

   # Find Volume ID
   volid=`aws ec2 describe-volumes --filter Name=tag:Name,Values=*data --query "Volumes[].VolumeId" --region us-east-2 --output text`

   until [[ $vol_status == '"available"' ]]; do
      vol_status=$(aws ec2 describe-volumes --volume-ids $volid --query 'Volumes[0].State')
      sleep 5
   done

   # Now that the volume is available, you can attach the existing "data" volume
   # Attach to device not in use.
   aws ec2 attach-volume --device /dev/sdf --instance-id $ec2id --volume-id $volid
   
   # Pause 120 seconds to make sure attached volume is ready. 
   # Note: If you do this too early you will not be able to mount correctly in the next steps.
   echo "Pause 120 seconds to make sure attached volume is ready." >> sanitycheck.txt
   date >> sanitycheck.txt
   sleep 120
   date >> sanitycheck.txt; echo ""

   # Create gitlab directory
   mkdir /gitlab
   # Mount the "data" volume
   mount /dev/nvme1n1 /gitlab/

   # Create and start docker container
   docker run --detach \
      --hostname dev-rb.leadingbit.net \
      --publish 443:443 --publish 80:80 \
      --name devrb \
      --restart always \
      --volume /gitlab/config:/etc/gitlab \
      --volume /gitlab/logs:/var/log/gitlab \
      --volume /gitlab/data:/var/opt/gitlab \
      --shm-size 256m \
      gitlab/gitlab-ce:latest

   # Pause 180 seconds to make sure container can be used. 
   echo "Pause 180 seconds to make sure container can be used." >> sanitycheck.txt
   date >> sanitycheck.txt
   sleep 180
   date >> sanitycheck.txt; echo ""
   echo "Gitlab is ready." >> sanitycheck.txt;
fi
#else
   # Put an xfs file system on attached EBS data volume the first time only
   # mkfs -t xfs /dev/nvme1n1
#fi
