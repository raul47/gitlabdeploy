lbs_config.yaml - This is the configuration file. Each value will be parsed to be read in as a parameter.
For example
# Tags
tags:
  application: gitlab
  company: lbs
  key: Name
  suffix:
    # Autoscaling Group
    asg: asg
    boot: boot
    data: data
    # Launch Template
    lt: lt
    ltebs: ltebs
  datetimestamp: 02-05-2023-0715
  value: ec2-demo
  version: v1

List of config file parameters:
tags_application="gitlab"
tags_company="lbs"
tags_key="Name"
tags_suffix_asg="asg"
tags_suffix_boot="boot"
tags_suffix_data="data"
tags_suffix_lt="lt"
tags_suffix_ltebs="ltebs"
tags_datetimestamp="02-05-2023-0715"
tags_value="ec2-demo"
tags_version="v1"

lbs-demo.sh - This is the script that will configure and set up the AWS environment. Currently, an initial account must exist with VPC, Security Group, and Subnets already existing. Future iteration will have this included in the code.
The script will:
-Update the aws command line verion
-Parse the config file
-Create an Elastic Block Storage volume
-Create and set up the Role 
-Create and set up the Instance Profile 
-Encode the userdata to base64 so that it can be passed to the Launch Template
-Create a Launch Template
-Create an Autoscaling group
-Store values in the Parameter Store

lbs-role-trust-policy.json - Trust policy that is needed when creating the role

lbs-userdata.sh - The code that is passed into the Launch Template to create the EC2 instance.
The script will:
-Create temporary credentials to set up the instance
-Tag the EC2 instance
-Install the Docker engine
-Create and mount the Gitlab directory
-Create and run the Gitlab Docker container 